package lab5;

import lab5.message.TaskMessage;

public class ClientConnectionInfo {

    private TaskMessage currentTask = null;
    private boolean ended = false;

    public long getLastConnectionTime() {
        return lastConnectionTime;
    }

    private long lastConnectionTime;

    TaskMessage getCurrentTask() {
        return currentTask;
    }

    void setCurrentTask(TaskMessage currentTask) {
        this.currentTask = currentTask;
        lastConnectionTime = System.currentTimeMillis();
    }

    boolean isEnded() {
        return ended;
    }

    void end() {
        this.ended = true;
    }
}
