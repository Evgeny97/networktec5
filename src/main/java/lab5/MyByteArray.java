package lab5;//Created by Evgeny on 01.11.2017.

import java.util.Arrays;

public class MyByteArray {
    private static char[] letters = {'A', 'B', 'C', 'D'};
    private byte[] array;
    private int lettersCount;

    public MyByteArray() {
        array = new byte[1];
        lettersCount = 0;
    }

    public MyByteArray(byte[] array, int lettersCount) {
        this.array = array;
        this.lettersCount = lettersCount;
    }

    public int getLettersCount() {
        return lettersCount;
    }

    String getWord() {
        StringBuilder stringBuilder = new StringBuilder(lettersCount);
        for (byte b : array) {
            for (int i = 0; i < 4; i++) {
                int letterChar = (b & 0b11000000) >>> 6;//192
                b <<= 2;
                stringBuilder.append(letters[letterChar]);
            }
        }
        if (stringBuilder.length() != lettersCount)
            stringBuilder.delete(0, stringBuilder.length() - lettersCount);
        return stringBuilder.toString();
    }

    void next() {
        if (lettersCount == 0) {
            lettersCount++;
            return;
        }
        boolean addLetter = true;
        for (int i = array.length - 1; i > 0; i--) {
            if (array[i] != (byte) 0b11111111) {
                addLetter = false;
                break;
            }
        }
        if (addLetter) {
            if ((array[0] == (byte) 0b00000011 && lettersCount % 4 == 1) || (array[0] == (byte) 0b00001111 && lettersCount % 4 == 2)
                    || (array[0] == (byte) 0b00111111 && lettersCount % 4 == 3)) {
                lettersCount++;
                Arrays.fill(array, (byte) 0);
                return;
            }
            if (array[0] == (byte) 0b11111111) {
                array = new byte[array.length + 1];
//                System.arraycopy(array, 0, newArray, newArray.length - array.length, array.length);
                // array = newArray;
                lettersCount++;
                Arrays.fill(array, (byte) 0);
                return;
            }
        }
        for (int i = array.length - 1; i >= 0; i--) {
            array[i]++;
            if (array[i] != 0) {
                break;
            }
        }
    }
}
