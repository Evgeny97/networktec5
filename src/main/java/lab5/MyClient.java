package lab5;

import lab5.message.AnswerMessage;
import lab5.message.BaseClientMessage;
import lab5.message.ClientConnectionMessage;
import lab5.message.TaskMessage;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.UUID;

public class MyClient {
    InetAddress serverAddress;
    int serverPort;
    UUID uuid = UUID.randomUUID();
    long stringsWhithoutPrint;
    private Socket socket = null;

    public MyClient(InetAddress serverAddress, int serverPort) throws IOException {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }

    public static void main(String[] args) {
        try {
            MyClient myClient = new MyClient(InetAddress.getByName(args[0]), Integer.valueOf(args[1]));
            myClient.startWork();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void startWork() throws IOException {
        try {
            openSocketAndSendMessage(new ClientConnectionMessage(uuid));
            while (true) {
                TaskMessage taskMessage;

                InputStream sin = socket.getInputStream();
                ObjectInputStream in = new ObjectInputStream(sin);
                taskMessage = (TaskMessage) in.readObject();


                if (taskMessage.getPostfixLetters() == -1) {
                    System.out.println("Closing");
                    break;
                }
                printStartString(taskMessage);
                MyByteArray myByteArray;
                int stopLettersCount = taskMessage.getPostfixLetters() + 1;
                stringsWhithoutPrint = 0;
                if (!taskMessage.getStartSequencePrefix().isEmpty()) {
                    myByteArray = new MyByteArray(new byte[(int) Math.ceil(taskMessage.getPostfixLetters() / 4.0)], taskMessage.getPostfixLetters());
                    while (myByteArray.getLettersCount() < stopLettersCount) {
                        String testString = taskMessage.getStartSequencePrefix() + myByteArray.getWord();
                        if (DigestUtils.md5Hex(testString).equals(taskMessage.getMd5Hash())) {
                            break;
                        }
                        printCurrString(testString);
                        myByteArray.next();
                    }
                } else {
                    myByteArray = new MyByteArray();
                    while (myByteArray.getLettersCount() < stopLettersCount) {
                        String testString = myByteArray.getWord();
                        if (DigestUtils.md5Hex(testString).equals(taskMessage.getMd5Hash())) {
                            break;
                        }
                        printCurrString(testString);
                        myByteArray.next();
                    }
                }
                if (myByteArray.getLettersCount() == stopLettersCount) {
                    openSocketAndSendMessage(new AnswerMessage(uuid, false, null));
                } else {
                    openSocketAndSendMessage(new AnswerMessage(uuid, true, taskMessage.getStartSequencePrefix() + myByteArray.getWord()));
                    System.out.println("Answer Found, Closing");
                    break;
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted");
            if (socket != null) {
                socket.close();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            if (socket != null) {
                socket.close();
            }
        }
    }

    private void printCurrString(String testString) {
        stringsWhithoutPrint++;
        if (stringsWhithoutPrint > 2500000) {
            stringsWhithoutPrint = 0;
            System.out.println("Work in progress, current test string is: " + testString);
        }
    }

    private void printStartString(TaskMessage taskMessage) {
        if (!taskMessage.getStartSequencePrefix().isEmpty()) {
            StringBuilder startString = new StringBuilder(taskMessage.getStartSequencePrefix());
            for (int i = 0; i < taskMessage.getPostfixLetters(); i++) {
                startString.append('A');
            }
            System.err.println("start working from String: " + startString.toString());
        } else {
            System.err.println("start working from String: ");
        }
    }

    private void openSocketAndSendMessage(BaseClientMessage clientMessage) throws InterruptedException {
        boolean messageSent = false;
        while (!messageSent) {
            try {
                socket = new Socket(serverAddress, serverPort);
                OutputStream sout = socket.getOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(sout);
                out.writeObject(clientMessage);
                messageSent = true;
            } catch (IOException e) {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                System.out.println("can't connect to server, sleep 3 sec");
                Thread.sleep(3000);
            }
        }
    }
}
