package lab5;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.UUID;

public class MyServer {
    private HashMap<UUID, ClientConnectionInfo> clients = new HashMap<>();
    private ServerSocket serverSocket;
    private boolean answerFound = false;
    private String answerString;

    public MyServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public static void main(String[] args) {
        try {
            //TaskProvider.getInstance().setMd5Cash(args[1]);
            TaskProvider.getInstance().setMd5Cash(DigestUtils.md5Hex("DDDCCCBBBAAAA"));
            MyServer myServer = new MyServer(Integer.valueOf(args[0]));
            myServer.startWork();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    HashMap<UUID, ClientConnectionInfo> getClients() {
        return clients;
    }

    void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    boolean isAnswerFound() {
        return answerFound;
    }

    void setAnswerFound() {
        this.answerFound = true;
    }

    void startWork() throws ClassNotFoundException, IOException {
        Socket clientSocket = null;

        serverSocket.setSoTimeout(1000);
        while (true) {
            try {

                //TODO new thread
                //TODO socket close good
                clientSocket = serverSocket.accept();
                NewSocketHandler socketHandler = new NewSocketHandler(this, clientSocket);
                socketHandler.run();

            } catch (SocketTimeoutException ignored) {
                if (answerFound) {
                    if (isAllEnded()) {
                        System.out.println("Server closing");
                        break;
                    }
                }
            }

        }

//            if (clientSocket != null) {
//                clientSocket.close();
//            }


    }

    private boolean isAllEnded() {
        boolean isAllEnded = true;
        for (HashMap.Entry<UUID, ClientConnectionInfo> entry : clients.entrySet()) {
            if (!entry.getValue().isEnded() && (System.currentTimeMillis() - entry.getValue().getLastConnectionTime() < 30000)) {
                isAllEnded = false;
                break;
            }
        }
        return isAllEnded;
    }
}
