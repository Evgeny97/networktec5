package lab5;
//Created by Evgeny on 15.11.2017.

import lab5.message.AnswerMessage;
import lab5.message.BaseClientMessage;
import lab5.message.ClientConnectionMessage;
import lab5.message.TaskMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class NewSocketHandler extends Thread {
    private MyServer myServer;
    private Socket socket;

    NewSocketHandler(MyServer myServer, Socket socket) {
        this.myServer = myServer;
        this.socket = socket;
    }

    @Override
    public void run() {
        try (Socket socket2 = socket) {
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            BaseClientMessage baseClientMessage = (BaseClientMessage) objectInputStream.readObject();
            ClientConnectionInfo foundClientConnectionInfo;
            if ((foundClientConnectionInfo = myServer.getClients().get(baseClientMessage.getUuid())) != null) {
                workWithFoundConnectionInfo(baseClientMessage, foundClientConnectionInfo);
            } else {
                workWithNotFoundConnectionInfo(baseClientMessage);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void workWithNotFoundConnectionInfo(BaseClientMessage baseClientMessage) throws IOException {
        if (baseClientMessage instanceof ClientConnectionMessage) {
            ClientConnectionInfo clientConnectionInfo = new ClientConnectionInfo();
            myServer.getClients().put(baseClientMessage.getUuid(), clientConnectionInfo);
            sendNextMessage(clientConnectionInfo);
        } else {
            throw new RuntimeException("No such UUID exist");
        }
    }

    private void workWithFoundConnectionInfo(BaseClientMessage baseClientMessage, ClientConnectionInfo foundClientConnectionInfo) throws IOException {
        if (baseClientMessage instanceof AnswerMessage) {
            TaskProvider.getInstance().taskCompleted(foundClientConnectionInfo.getCurrentTask());
            AnswerMessage answerMessage = (AnswerMessage) baseClientMessage;
            if (answerMessage.isAnswerFound()) {
                myServer.setAnswerFound();
                String answerString = answerMessage.getAnswerString();
                myServer.setAnswerString(answerString);
                System.out.println("Answer is: " + answerString);
            }
            sendNextMessage(foundClientConnectionInfo);
        } else {
            throw new RuntimeException("New Connection with already existing UUID");
        }
    }

    private void sendNextMessage(ClientConnectionInfo foundClientConnectionInfo) throws IOException {
        TaskMessage nextTask;
        if (!myServer.isAnswerFound()) {
            nextTask = TaskProvider.getInstance().getTask();
        } else {
            nextTask = TaskProvider.CLOSING_MESSAGE;
        }
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(nextTask);
        foundClientConnectionInfo.setCurrentTask(nextTask);
        if (nextTask == TaskProvider.CLOSING_MESSAGE) {
            foundClientConnectionInfo.end();
        }
    }
}
