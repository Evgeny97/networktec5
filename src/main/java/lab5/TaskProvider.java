package lab5;

import lab5.message.TaskMessage;

import java.util.HashMap;

public class TaskProvider {
    public static final TaskMessage CLOSING_MESSAGE = new TaskMessage(null, null, -1);
    private static final int postfixLetters = 8;
    private static TaskProvider taskProvider = new TaskProvider();
    private String md5Cash;
    private MyByteArray myByteArray = new MyByteArray();
    private HashMap<TaskMessage, Long> tasksInProgress = new HashMap<>();

    private TaskProvider() {

    }

    static TaskProvider getInstance() {
        return taskProvider;
    }

    public synchronized void taskCompleted(TaskMessage completedTask) {
        tasksInProgress.remove(completedTask);
    }

    public void setMd5Cash(String md5Cash) {
        this.md5Cash = md5Cash;
    }

    public synchronized TaskMessage getTask() {
        long curTime = System.currentTimeMillis();
        for (HashMap.Entry<TaskMessage, Long> entry : tasksInProgress.entrySet()) {
            if (curTime - entry.getValue() > 120000) {
                entry.setValue(curTime);
                return entry.getKey();
            }
        }
        TaskMessage taskMessage = new TaskMessage(md5Cash, myByteArray.getWord(), postfixLetters);
        myByteArray.next();
        tasksInProgress.put(taskMessage, System.currentTimeMillis());
        return taskMessage;
    }
}
