package lab5.message;

import java.io.Serializable;
import java.util.UUID;

public class AnswerMessage extends BaseClientMessage implements Serializable {
    private String answerString;
    private boolean isAnswerFound;

    public AnswerMessage(UUID uuid,boolean isAnswerFound, String answerString) {
        super(uuid);
        this.answerString = answerString;
        this.isAnswerFound = isAnswerFound;
    }

    public String getAnswerString() {
        return answerString;
    }

    public boolean isAnswerFound() {
        return isAnswerFound;
    }
}
