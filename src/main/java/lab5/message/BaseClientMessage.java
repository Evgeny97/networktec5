package lab5.message;
//Created by Evgeny on 08.11.2017.

import java.io.Serializable;
import java.util.UUID;

public class BaseClientMessage extends BaseMessage implements Serializable {
    private UUID uuid;// = UUID.randomUUID();

    public BaseClientMessage(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}
