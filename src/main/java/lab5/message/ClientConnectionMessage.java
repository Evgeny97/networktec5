package lab5.message;
//Created by Evgeny on 08.11.2017.

import java.io.Serializable;
import java.util.UUID;

public class ClientConnectionMessage extends BaseClientMessage implements Serializable{

    public ClientConnectionMessage(UUID uuid) {
        super(uuid);
    }
}
