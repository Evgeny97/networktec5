package lab5.message;

import java.io.Serializable;

public class TaskMessage extends BaseMessage implements Serializable {
    private String md5Hash;
    private String startSequencePrefix;
    private int postfixLetters;// -1 - close

    public TaskMessage(String md5Hash, String startSequencePrefix, int postfixLetters) {
        this.md5Hash = md5Hash;
        this.startSequencePrefix = startSequencePrefix;
        this.postfixLetters = postfixLetters;
    }

    public String getStartSequencePrefix() {
        return startSequencePrefix;
    }

    public String getMd5Hash() {
        return md5Hash;
    }

    public int getPostfixLetters() {
        return postfixLetters;
    }
}
